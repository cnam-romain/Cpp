#include <iostream>

using namespace std;

int main(){
    srand(time(NULL));    
    int nb = rand()%1001;
    int count = 0;

    int choice;
    bool valid = false;
    do{
        count++;
        printf("Nombre : ");        
        cin >> choice;
        if(choice == nb){ 
            printf("\nBravo (%d partie(s))\n", count);
            valid = true;
        }
        else if( choice > nb){
            printf("    -> Plus petit\n");
        }
        else{
            printf("    -> Plus grand\n");
        }
    }
    while (!valid);      
    

    return EXIT_SUCCESS;
}
