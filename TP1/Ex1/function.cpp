#include "function.hpp"


// I.1.2
void inverse(int& a, int& b){
    int temp = a;
    a = b;
    b = temp;    
}

void print(int* tab, int size){
    printf("[");
    for(int i=0; i<size; i++){
        printf("%d", tab[i]);
        if(i<size-1){printf(", ");}
    }
    printf("]\n");
}
