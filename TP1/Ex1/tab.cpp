#include "function.hpp" 

void fill_array(int* tab, int size, int max){
    for(int i=0; i<size; i++){
        tab[i] = rand() % max;
    }
}

// sortType = 1 -> croissant
// sortType = 2 -> decroissant
void sort(int* tab, int size, int sortType){  
    long intervalle = size;
    bool echange = true;
    int i;    
    while (intervalle>1 || echange)
    {
        intervalle = (long)intervalle/1.3;
        if( intervalle < 1){ intervalle = 1; }
        i=0;
        echange = false;
        while (i<size-intervalle)
        {
            switch (sortType)
            {
                case 1: echange = tab[i] > tab[i+intervalle]; break;
                case 2: echange = tab[i] < tab[i+intervalle]; break;
                default: break;
            }   

            if(echange){
                inverse(tab[i], tab[i+intervalle]); 
            }
            i++;
        }   
    } 
}

void reverse(int* tab, int size){
    for(int i=0; i<size/2; i++){
        inverse(tab[i], tab[size-1-i]);
    }
}


int main(){
    srand(time(NULL));
    int size;
    printf("Taille du tableau : ");
    std::cin >> size;

    int tab[size];
    fill_array(tab, size, 20);
    print(tab, size);

    int sortType;
    do{
        printf("Type de tri (croissant:1 / decroissant:2) : ");
        std::cin >> sortType;        
    }
    while (sortType != 1 && sortType != 2);
    
    sort(tab, size, sortType);
    print(tab, size);
    reverse(tab, size);
    print(tab, size);
    return EXIT_SUCCESS;
}

