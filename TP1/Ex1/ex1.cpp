#include "function.hpp"

// I.1.1
int somme(int a, int b){
    return a+b;
}

// I.1.2 -> function.cpp 


// I.1.3
void sommePtr(int a, int b, int* sum){
    *sum = a+b;
}
void sommeRef(int a, int b, int& sum){
    sum = a+b;
}

// I.1.4 -> tab.cpp

int main(){
    int a = 4;
    int b = 5;
    int c = 10;
    int d = 2;
    int sum;

    printf("Somme classique %d\n", somme(a,b));  

    // Somme avec pointeur
    sommePtr(a,b, &sum);
    printf("Somme pointeur (%d+%d): %d\n",a,b, sum ); 

    // Somme avec référence
    sommeRef(c,d, sum);
    printf("Somme reference (%d+%d): %d\n",c,d, sum ); 

    // Inverse
    printf("a=%d b=%d\n", a, b);
    inverse(a, b);
    printf("a=%d b=%d\n", a, b);

    return EXIT_SUCCESS;        
}