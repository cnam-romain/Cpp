#include "forme.hpp"


Rectangle::Rectangle(int longueur, int largeur, Point coin){    
    this->longueur = longueur;
    this->largeur = largeur;    
    this->coin = coin;
}


int Rectangle::GetLargeur() const { return this->largeur; }
void Rectangle::SetLargeur(int largeur){ this->largeur = largeur;}

int Rectangle::GetLongueur() const {  return this->longueur; }
void Rectangle::SetLongueur(int longueur){ this->longueur = longueur; }


Point Rectangle::GetCoin() const { return this->coin; }
void Rectangle::SetCoin(Point point){ this->coin = point; }


int Rectangle::Perimetre() const {    
    return (this->largeur + this->longueur)*2;
}
int Rectangle::Surface() const { 
    return this->largeur * this->longueur;
}


bool Rectangle::PlusGrandPerimetre(const Rectangle& rect) const {    
    return this->Perimetre() > rect.Perimetre();
}
bool Rectangle::PlusGrandeSurface(const Rectangle& rect) const {
    return this->Surface() > rect.Surface();
}

void Rectangle::Afficher() const{    
    std::cout << "Rectangle : " << std::endl
        << "    Longueur : "  << this->longueur        << std::endl
        << "    Largeur : "   << this->largeur         << std::endl
        << "    Point : "     << this->coin.Afficher() << std::endl
        << "    Perimètre : " << this->Perimetre()     << std::endl
        << "    Surface : "   << this->Surface()       << std::endl;
}


