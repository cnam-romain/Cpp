#ifndef forme_h
#define forme_h

#include "math.h"
#include <iostream>
#include <vector>


struct Point {
    float x;
    float y;

    Point(){}
    Point(float x, float y){
        this->x = x;
        this->y = y;
    }

    std::string Afficher() const{
        return "("+ std::to_string(x) +", "+ std::to_string(y) +")";
    }    

    inline static float DistanceEntre(const Point& point1, const Point& point2){                      
        return sqrt( pow(point2.x - point1.x, 2) + pow(point2.y - point1.y, 2) );
    }
};



class Rectangle {

    private:
        int longueur;
        int largeur;
        Point coin;

    public:
        Rectangle();
        Rectangle(int longueur, int largeur, Point coin);

        inline int GetLongueur() const;
        inline void SetLongueur(int longueur);

        inline int GetLargeur() const;
        inline void SetLargeur(int largeur);
        
        inline Point GetCoin() const;
        void SetCoin(Point point);

        inline int Perimetre() const;
        inline int Surface() const;

        bool PlusGrandPerimetre(const Rectangle& rect) const;
        bool PlusGrandeSurface(const Rectangle& rect) const;

        void Afficher() const;
};



class Cercle {

    private:
        int diametre;
        float rayon;
        Point centre;
        
    public:
        Cercle();
        Cercle(int diametre, Point center);

        inline int GetDiametre() const;        
        inline void SetDiametre(int diametre);

        inline Point GetCentre() const;
        void SetCentre(Point point);

        inline float Perimetre() const;
        inline float Surface() const;

        bool EstDessus(const Point& point) const;
        bool EstInclue(const Point& point) const;

        void Afficher() const;
};



class Triangle {

    private:
        Point point1;
        Point point2;
        Point point3;              

    public:

        Triangle();
        Triangle(Point point1, Point point2, Point point3);

        inline Point GetPoint1() const;
        void SetPoint1(Point point);

        inline Point GetPoint2() const;
        void SetPoint2(Point point);

        inline Point GetPoint3() const;
        void SetPoint3(Point point);

        float Base() const;
        inline float Hauteur() const;
        float Surface() const;
        std::vector<float> Longueurs() const;

        bool EstIsocele() const;
        bool EstRectangle() const;
        bool EstEquilateral() const;

        void Afficher() const;

};

#endif // forme_h