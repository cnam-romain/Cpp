#include <stdlib.h>
#include "forme.hpp"


void TestTriangle(){
    Triangle triangle(
        Point(0,0),
        Point(5,3),
        Point(1,-2.5)
    );
    std::cout << "Triangle Lambda "<< std::endl;    
    triangle.Afficher();   
    
    std::cout << std::endl;

    Triangle triangleIsocele(
        Point(0,0),
        Point(2,5),
        Point(4,0)
    );         
    std::cout << "Triangle Isocèle "<< std::endl;    
    triangleIsocele.Afficher();
    
    std::cout << std::endl;

    Triangle triangleRectangle(
        Point(0,0),
        Point(1,1),
        Point(2,-2)
    );         
    std::cout << "Triangle Rectangle "<< std::endl;
    triangleRectangle.Afficher();

    std::cout << std::endl;

    Triangle triangleEquilateral(
        Point(0,0),
        Point(1,1.733),
        Point(2,0)
    );         
    std::cout << "Triangle Equilateral "<< std::endl;
    triangleEquilateral.Afficher();
}

void TestCercle(){
    Cercle cercle(10, Point(0,0));
    std::cout << "Cercle de référence" << std::endl;
    cercle.Afficher();      

    std::cout << std::endl;

    Point point(5,0);
    std::cout << "Position du point de test : "<< point.Afficher() << std::endl;
    
    std::cout << std::endl;

    std::cout << "Point est sur le cercle de référence : " << cercle.EstDessus(point) << std::endl;
    std::cout << "Point est inclue dans le cercle de référence : " << cercle.EstInclue(point) << std::endl;
    
    std::cout << std::endl;
    
    point.x = 4.99;
    std::cout << "Nouvelle position du point de test : "<< point.Afficher() << std::endl;

    std::cout << "Point est sur le cercle de référence : " << cercle.EstDessus(point) << std::endl;
    std::cout << "Point est inclue dans le cercle de référence : " << cercle.EstInclue(point) << std::endl;
}

void TestRectangle(){
    Rectangle rectangle(10, 4, Point(0,0));
    std::cout << "Rectangle de référence" << std::endl;
    rectangle.Afficher();

    std::cout << std::endl;
    
    Rectangle rectangle2(9, 5, Point(1,0));
    std::cout << "Rectangle de test" << std::endl;
    rectangle2.Afficher();

    std::cout << std::endl;
    std::cout << "Rectangle de référence à un pérmimetre plus grand : " << rectangle.PlusGrandPerimetre(rectangle2) << std::endl;    
    std::cout << "Rectangle de test à une surface plus grande : " << rectangle2.PlusGrandeSurface(rectangle) << std::endl;

}


int main(int argc, char* argv[]){

    if(argc < 2){ 
        std::cout << "./main [0->rectangle | 1->cercle | 2->triangle]" << std::endl;
        return EXIT_FAILURE;
    }

    int choice = atoi(argv[1]);
    switch (choice)
    {
        case 0:
            TestRectangle();        
            break;
        case 1:
            TestCercle();
            break;
        case 2:
            TestTriangle();
            break;    
        default:
            break;
    }

    return EXIT_SUCCESS;
}