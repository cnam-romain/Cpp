#ifndef structs_h
#define structs_h

struct Position
{
    int line;
    int column;

    Position(){};
    Position(int line, int column){
        this->line = line;
        this->column = column;
    }
    static bool isEqual(const Position& a, const Position& b){
        return (a.line == b.line && a.column == b.column);
    }
};


#endif // structs_h