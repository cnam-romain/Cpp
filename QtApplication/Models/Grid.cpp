#include "Grid.h"

Grid::Grid(){}

Grid::Grid(int nbLines, int nbColumns){
    CreateGrid(nbLines, nbColumns); 
}

void Grid::Clear(){
    std::fill(grid.begin(),grid.end(),DEFAULT_CHAR);
}

bool Grid::Place(Position position, char value){
    if( !PositionInBounds(position) ){
        return false;
    }

    if( !IsEmpty(position) ){         
        return false;
    }

    grid[position.line * GetColumnNumber() + position.column] = value;
    return true;
}


std::vector<Position> Grid::GetEmptyCells() const {
    std::vector<Position> list;
    for(int i=0; i<grid.size(); i++){
        if(IsEmpty(i)){
            list.push_back(
                    Position(i/GetColumnNumber(), i%GetColumnNumber())
            );
        }
    }
    return list;
}

std::vector<char> Grid::GetCellContentFromLine(int line) const {
    return GetCellContentFromLine(Position(line, 0), GetColumnNumber());
}
std::vector<char> Grid::GetCellContentFromLine(Position startPosition, int cellCount) const {
    std::vector<char> list;    
    if(startPosition.column+cellCount > GetColumnNumber()){
        cellCount = GetColumnNumber(); 
    }
    for(int column=startPosition.column; column<cellCount; column++){
        list.push_back(
            grid[startPosition.line*GetColumnNumber() + column]
        );
    }
    return list;
}

std::vector<char> Grid::GetCellContentFromColumn(int column) const {
    return GetCellContentFromColumn(Position(0, column), GetLineNumber());
}
std::vector<char> Grid::GetCellContentFromColumn(Position startPosition, int cellCount) const {
    std::vector<char> list;
    if(startPosition.line + cellCount > GetLineNumber()){
        cellCount = GetLineNumber();
    }
    for(int line=startPosition.line; line<cellCount; line++){
        list.push_back(
            grid[line*GetColumnNumber() + startPosition.column]
        );
    }
    return list;
}

std::vector<char> Grid::GetCellContentFromAscendingDiagonal(Position startPosition) const {
    std::vector<char> list;
    while (PositionInBounds(startPosition))
    {
        list.push_back(
            grid[ startPosition.line*GetColumnNumber() + startPosition.column ]
        );
        startPosition.line--;
        startPosition.column++;
    }    
    return list;
}

std::vector<char> Grid::GetCellContentFromDescendingDiagonal(Position startPosition) const {
    std::vector<char> list;
    while (PositionInBounds(startPosition))
    {
        list.push_back(
            grid[ startPosition.line*GetColumnNumber() + startPosition.column ]
        );
        startPosition.line++;
        startPosition.column++;
    }    
    return list;
}

std::vector<char> Grid::GetNeighbors(Position position) const{
    int adjacent[8][2] = {
                    {-1, -1,}, 
                    { 0, -1 },
                    { 1, -1 },
                    {-1,  0 },
                    { 1,  0 },
                    {-1,  1 }, 
                    { 0,  1 },
                    { 1,  1 }
        };

    std::vector<char> neighbors;
    for (auto & offset : adjacent){
        Position offsetPosition = position;
        
        offsetPosition.line += offset[1];
        offsetPosition.column += offset[0];
        
        if(PositionInBounds(offsetPosition)){
            neighbors.push_back(GetCell(offsetPosition));
        }
    }
    return neighbors;
}

std::vector<Position> Grid::GetUpNeighborsPosition(Position position) const{
    int adjacent[2][2] = {
            {-1, -1},
            { -1,  1 }
    };

    std::vector<Position> neighbors;
    for (auto & offset : adjacent){
        Position offsetPosition = position;

        offsetPosition.line += offset[0];
        offsetPosition.column += offset[1];

        if(PositionInBounds(offsetPosition)){
            neighbors.push_back((offsetPosition));
        }
    }
    return neighbors;
}


std::vector<Position> Grid::GetNeighPosFromAscendingDiagonalLeft(Position startPosition) const{
    std::vector<Position> list;
    while (PositionInBounds(startPosition))
    {
        startPosition.line--;
        startPosition.column--;
        list.emplace_back(startPosition.line, startPosition.column);
    }
    return list;
}

std::vector<Position> Grid::GetNeighPosFromAscendingDiagonalRight(Position startPosition) const{
    std::vector<Position> list;
    while (PositionInBounds(startPosition))
    {
        startPosition.line--;
        startPosition.column++;
        list.emplace_back(startPosition.line, startPosition.column);
    }
    return list;
}


std::vector<Position> Grid::GetNeighPosFromDescendingDiagonalLeft(Position startPosition) const{
    std::vector<Position> list;
    while (PositionInBounds(startPosition))
    {
        startPosition.line++;
        startPosition.column--;
        list.emplace_back(startPosition.line, startPosition.column);
    }
    return list;
}


std::vector<Position> Grid::GetNeighPosFromDescendingDiagonalRight(Position startPosition) const{
    std::vector<Position> list;
    while (PositionInBounds(startPosition))
    {
        startPosition.line++;
        startPosition.column++;
        list.emplace_back(startPosition.line, startPosition.column);
    }
    return list;
}


std::vector<Position> Grid::GetDownNeighborsPosition(Position position) const{
    int adjacent[2][2] = {
            {1, -1},
            { 1,  1 }
    };

    std::vector<Position> neighbors;
    for (auto & offset : adjacent){
        Position offsetPosition = position;

        offsetPosition.line += offset[0];
        offsetPosition.column += offset[1];

        if(PositionInBounds(offsetPosition)){
            neighbors.push_back((offsetPosition));
        }
    }
    return neighbors;
}

Position Grid::GetDownRightPosition(Position position) const {
    if(!PositionInBounds({position.line + 1, position.column + 1})){
        return {-1,-1};
    }
    return {position.line + 1, position.column + 1};
}

Position Grid::GetDownLeftPosition(Position position) const{
    if(!PositionInBounds({position.line + 1, position.column - 1})){
        return {-1,-1};
    }
    return {position.line + 1, position.column - 1};
}

Position Grid::GetUpRightPosition(Position position) const {
    if(!PositionInBounds({position.line - 1, position.column + 1})){
        return {-1,-1};
    }
    return {position.line - 1, position.column + 1};
}

Position Grid::GetUpLeftPosition(Position position) const {
    if(!PositionInBounds({position.line - 1, position.column - 1})){
        return {-1,-1};
    }
    return {position.line - 1, position.column - 1};
}
std::vector<char> Grid::GetNeighbors(Position position, char symbol) const {
    std::vector<char> filteredNeighbors;
    for(char cell : GetNeighbors(position)){
        if(cell != symbol && cell != DEFAULT_CHAR){
            filteredNeighbors.push_back(cell);
        }
    }
    return filteredNeighbors;
}

bool Grid::Set(Position position, char value) {
    if( !PositionInBounds(position) ){
        return false;
    }
    grid[position.line * GetColumnNumber() + position.column] = value;
    return true;
}

bool Grid::Set(int index, char value) {
    if( index < 0 || index > grid.size() ){
        return false;
    }
    grid[index] = value;
    return true;
}
std::vector<Position> Grid::GetAllPositionBySymbol(char symbol) const {
    auto res = std::vector<Position>();
    for(int col = 0; col < this->GetColumnNumber(); col++){
        for(int line = 0; line < this->GetLineNumber(); line++){
            if(this->GetCell({line,col}) == symbol)
                res.emplace_back(line,col);
        }
    }
    return res;
}

//
// PRIVATE
//

void Grid::CreateGrid(int line, int column){
    this->dimensions = std::pair<int,int>{line, column};
    this->grid = std::vector<char>(line * column, DEFAULT_CHAR);
}

int Grid::GetSymbolCount(char symbol) const {
    int count = 0;
    for (auto cell: grid)
        if(cell == symbol) count ++;
    return count;
}

