#ifndef grid_h
#define grid_h

#include <vector>
#include <string>
#include "Structs.h"

class Grid {

public :
    char DEFAULT_CHAR = ' ';

    Grid();
    Grid(int nbLines, int nbColumns);

    int inline GetColumnNumber() const { return dimensions.second; }
    int inline GetLineNumber() const { return dimensions.first; }

    char inline GetCell(Position position) const {
        int index = position.line * GetColumnNumber() + position.column;
        if (index > GetColumnNumber() * GetLineNumber() || index < 0)
            return '\t';
        return grid[index];
    }
    char inline GetCell(int index) const {
        if (index > GetColumnNumber() * GetLineNumber() || index < 0)
            return DEFAULT_CHAR;
        return grid[index];
    }

    bool inline IsEmpty(Position position) const { return grid[position.line * GetColumnNumber() + position.column] == DEFAULT_CHAR; }
    bool inline IsEmpty(int index) const { return grid[index] == DEFAULT_CHAR; }

    std::vector<Position> GetEmptyCells() const;

    std::vector<char> GetCellContentFromLine(int line) const;
    std::vector<char> GetCellContentFromLine(Position startPosition, int cellCount) const;

    std::vector<char> GetCellContentFromColumn(int column) const;
    std::vector<char> GetCellContentFromColumn(Position startPosition, int cellCount) const;

    std::vector<char> GetCellContentFromAscendingDiagonal(Position startPosition) const;
    std::vector<char> GetCellContentFromDescendingDiagonal(Position startPosition) const;

    std::vector<Position> GetNeighPosFromAscendingDiagonalLeft(Position startPosition) const;
    std::vector<Position> GetNeighPosFromAscendingDiagonalRight(Position startPosition) const;
    std::vector<Position> GetNeighPosFromDescendingDiagonalLeft(Position startPosition) const;
    std::vector<Position> GetNeighPosFromDescendingDiagonalRight(Position startPosition) const;

    std::vector<Position> GetAllPositionBySymbol(char symbol) const;

    std::vector<char> GetNeighbors(Position position) const;
    std::vector<Position> GetUpNeighborsPosition(Position position) const;
    std::vector<Position> GetDownNeighborsPosition(Position position) const;
    // retourne tous les voisins qui ne sont pas le symbol
    std::vector<char> GetNeighbors(Position position, char symbol) const;

    Position GetDownRightPosition(Position position) const;
    Position GetDownLeftPosition(Position position) const;
    Position GetUpRightPosition(Position position) const;
    Position GetUpLeftPosition(Position position) const;

    void inline ToSpecialSymbol(Position position) {
        auto temp = GetCell(position);
        Set(position,toupper(temp));
    }
    bool inline isSymbolSpecial(Position position){
        auto temp = GetCell(position);
        return  isupper(temp);
    }

    int GetSymbolCount(char symbol) const;

    bool Place(Position position, char value);
    bool Set(int index, char value);
    bool Set(Position position, char value);
    void Clear();


private :
    std::vector<char> grid;
    std::pair<int, int> dimensions;

    void CreateGrid(int nbLines, int nbColumns);
    inline bool PositionInBounds(Position position) const {
        return !(
                position.line < 0 || position.line >= GetLineNumber() ||
                position.column < 0 || position.column >= GetColumnNumber()
        );
    }
};

#endif //grid_h
