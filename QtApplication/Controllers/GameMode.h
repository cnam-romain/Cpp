#ifndef GameMode_h
#define GameMode_h

#include <vector>
#include <iostream>
#include <memory>
#include "../Models/Player.h"
#include "../Models/Grid.h"
#include "../Views/UserInterface.h"

class GameController;

enum GameState {
    DEFEAT,
    WIN,
    EQUALITY,
    NOTHING
};


class GameMode{
    public :
        virtual inline int GetPlayerNumber() const = 0;
        virtual inline int GetGridColumnNumber() const = 0;
        virtual inline int GetGridLineNumber() const = 0;
        virtual inline bool IsBotEnable() const = 0;
        virtual inline std::string GetGameName() const = 0;

        virtual void GridInitState() const = 0;
        virtual void Play() = 0;
        virtual void AIPlay() const = 0;
        virtual GameState GridState() const = 0;
};



#endif //GameMode_h
