#ifndef GameController_h
#define GameController_h


#include <cstring>
#include <memory>
#include "../Models/Grid.h"
#include "../Models/Player.h"
#include "../Models/Structs.h"
#include "../Utils/Sauvegarder.h"

#include "GameMode.h"

#include "GameModes/Morpion.h"
#include "GameModes/Puissance4.h"
#include "GameModes/Ladies.h"
#include "GameModes/Othello.h"

#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QString>

#include <QEventLoop>
#include <qobject.h>
#include <iostream>


class GameController : public QEventLoop {
Q_OBJECT
public:
        
    static std::shared_ptr<GameController> GetInstance();
    void operator=(const GameController&) = delete;
    GameController(GameController& other) = delete;

    inline std::shared_ptr<UserInterface> GetUserInterface() const{
        return this->userInterface;
    }
    inline void SetUserInterface(std::shared_ptr<UserInterface> userInterface){
        this->userInterface = userInterface;
    }

    inline std::vector<Player> GetPlayers() const {
        return this->players;
    }

    /*inline Position GetLastPosition() const {
        return this->lastPosition;
    }*/

    inline Player GetCurrentPlayer() const {
        return this->players[currentPlayerIndex];            
    }

    inline Grid& GetGrid() {
        return grid;
    }

    inline bool IsFirst(const Player& player) const {
        return player.GetSymbol() == syms[0];
    }
    inline void SamePlayerForNextRound(){
        nextPlayer = false;
    }

    Player GetOtherPlayer(const Player& player) const;  
    
    void Exec();

    Position RequestInputByXY();
    Position RequestInputByLine();
    Position RequestInputByColumn();    


signals:
    void CreateNewGame_Signal();
    void LoadGame_Signal(std::string file);
    void GameChosed_Signal(int gameId);
    void CreatePlayer_Signal(std::string nameP1, bool isBotP1, std::string nameP2, bool isBotP2);    
    void PlayerInput_Signal(Position pos);
    void GoToEndPage_Signal();
    void Replay_Signal();
    void ReplayWithNewPlayers_Signal();
    void Exit_Signal();    
    void Save_Signal(std::string path);

    void Quit_loop();

public slots:
    void CreateNewGame_Action();
    void LoadGame_Action(std::string file);
    void GameChosed_Action(int gameId);
    void CreatePlayer_Action(std::string nameP1, bool isBotP1, std::string nameP2, bool isBotP2);
    void PlayerInput_Action(Position pos);
    void GoToEndPage_Action();
    void Replay_Action();
    void ReplayWithNewPlayers_Action();
    void Exit_Action();
    void Save_Action(std::string path);



private:

    explicit GameController(QObject * parent = nullptr){}

    std::vector<char> syms = {
            'x',
            'y'
    };

    static std::shared_ptr<GameController> _instance;
    //static GameController* _instance;

    std::shared_ptr<GameMode> gameType;
    std::vector<Player> players;
    std::shared_ptr<UserInterface> userInterface;
    Grid grid;

    Position inputPosition;
    int currentPlayerIndex;
    int gameId;        
    bool nextPlayer = true;

    void PlayGame();     
    void PlayRound();

    void FindWinner();
    bool FindGameState(const Player& player);              

    QJsonObject ToJson();

    void WaitForInputSignal();

    void LoadPlayers(QJsonArray players);
    void LoadGrid(QJsonArray grid);

    void InitGameMode(int gameId);
    void CreatePlayer(std::string name, char symbol, bool isBot);
    void InitGrid();
                
        
};

#endif //GameController_h
