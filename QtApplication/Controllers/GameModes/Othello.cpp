#include <random>
#include <functional>
#include "Othello.h"
#include "../GameController.h"

//
// PUBLIC
//

Othello::Othello(){}

void Othello::Play() {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    std::shared_ptr<UserInterface> UserInterface = ctrl->GetUserInterface();    
    Player player = ctrl->GetCurrentPlayer();
    char enemyChar = ctrl->GetOtherPlayer(player).GetSymbol();

    Grid& grid = ctrl->GetGrid();    

    bool isCorrect = false;
    Position inputPosition;
    do{ 
        inputPosition = GameController::GetInstance()->RequestInputByXY();
        if(!grid.IsEmpty(inputPosition)){ 
            UserInterface->DisplayWarningMessage("Position incorrect");
        }
        else {             
            if( EstSandwichVertical(grid, inputPosition, player.GetSymbol(),enemyChar) +
                EstSandwichHorizontal(grid, inputPosition, player.GetSymbol(), enemyChar) + 
                EstSandwichDiagonal(grid, inputPosition, player.GetSymbol(), enemyChar) ){
                isCorrect = true;
            }
            else{
                UserInterface->DisplayWarningMessage("Position incorrect");
            }
        }        
    }while(!isCorrect);

    grid.Place(inputPosition, player.GetSymbol());
    
}

void Othello::AIPlay() const {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    std::random_device r;
    std::vector<Position> emptyCells = ctrl->GetGrid().GetEmptyCells();
    auto gen = std::bind(std::uniform_int_distribution<>(0,emptyCells.size()),std::default_random_engine(r()));
    Position pos;
    bool isCorrect = false;
    Player player = ctrl->GetCurrentPlayer();
    while(!isCorrect){
        pos = emptyCells[gen()];
        auto neighs = ctrl->GetGrid().GetNeighbors(pos,player.GetSymbol());
        for(auto neigh: neighs){
            if(neigh != player.GetSymbol() && neigh != ' '){
                isCorrect = true;
                break;
            }
        }
    }

    ctrl->GetGrid().Place(pos, player.GetSymbol());    
}

void Othello::GridInitState() const {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    Grid& grid = ctrl->GetGrid();
    grid.Set(Position(3,3), ctrl->GetPlayers()[0].GetSymbol());
    grid.Set(Position(4,4), ctrl->GetPlayers()[0].GetSymbol());
    
    grid.Set(Position(4,3), ctrl->GetPlayers()[1].GetSymbol());
    grid.Set(Position(3,4), ctrl->GetPlayers()[1].GetSymbol());    
}

GameState Othello::GridState() const {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    const Grid grid = ctrl->GetGrid();
    char symbol = ctrl->GetCurrentPlayer().GetSymbol();
    char enemy = ctrl->GetOtherPlayer(ctrl->GetCurrentPlayer()).GetSymbol();

    return GetGameState(grid, symbol, enemy);
}

//
// PRIVATE
//

GameState Othello::GetGameState(const Grid &grid, char symbol, char enemy) const {
    if (grid.GetSymbolCount(symbol) < (grid.GetColumnNumber() * grid.GetLineNumber())/2) return GameState::NOTHING;

    int compteur=0;
    int compteur2=0;
    for(int i = 0; i < grid.GetLineNumber(); i++){
        for(int j = 0; j < grid.GetColumnNumber(); j++){
            if(grid.GetCell({i,j}) == symbol){
                compteur++;
            }
        }
    }
    for(int i = 0; i < grid.GetLineNumber(); i++){
        for(int j = 0; j < grid.GetColumnNumber(); j++){
            if(grid.GetCell({i,j}) == enemy){
                compteur2++;
            }
        }
    }

    if(compteur > compteur2){
        return GameState::WIN;
    }
    else if (compteur == compteur2){
        return GameState::EQUALITY;
    }

    return GameState::NOTHING;
}

typedef void* Xx__getComputeQuantumBestPositionByXYFromCircularPain__xX;
typedef void* BonSoleil;
typedef GameController* LeMondeEstMechant;


bool Othello::EstSandwichVertical(Grid& grid, Position &position, char player, char enemy) const{
    Position position2;
    int line = position.line;
    int column = position.column;
    bool sandwich = false;
    int i;

    // EN HAUT
    if(line != 0) 
    {
        i = line-1; 
        if(grid.GetCell({i, column}) == enemy)
        {
            while(i > 0 && grid.GetCell({i,column}) == enemy && !grid.IsEmpty({i, column}))
            {
                i--;
            } 
            if(grid.GetCell({i,column}) == player)
            {
                sandwich = true;
                RetournementVertical(grid, column, i, line, player);
            }
        }
    }

    // EN BAS
    if(column != GRID_COLUMNS && line != GRID_LINES)
    { 
        i = line+1;
        if(grid.GetCell({i,column}) == enemy)
        {
            while(i < GRID_COLUMNS-1 && grid.GetCell({i,column}) != player && !grid.IsEmpty({i,column}))
            {
                i++;
            }
            if(grid.GetCell({i,column}) == player)
            {
                sandwich = true;
                RetournementVertical(grid, column, line, i, player);
            }
        } 
    }
    return sandwich;

}

bool Othello::EstSandwichHorizontal(Grid& grid, Position &position, char player, char enemy) const{ 
    int x = position.line;
    int y = position.column;
    bool sandwich = false;
    int j; 
    // A GAUCHE
    if(y != 0)
    {
        j=y-1;
        if(grid.GetCell({x,j}) == enemy)
        {
            while(j >= 0 && grid.GetCell({x,j}) == enemy && !grid.IsEmpty({x,j}))
            {
                j--;
            }
            if(grid.GetCell({x,j}) == player)
            {
                sandwich = true;
                RetournementHorizontal(grid, x, j, y, player);
            }
        }
    }
     // A DROITE
    if(y != GRID_COLUMNS)
    {
        j=y+1;
        if(grid.GetCell({x,j}) == enemy)
        {
            while(j < GRID_COLUMNS-1 && grid.GetCell({x,j}) != player && !grid.IsEmpty({x,j}))
            {
                j++;
            }
            if(grid.GetCell({x,j}) == player)
            {
                sandwich = true;
                RetournementHorizontal(grid, x, y, j, player);
            }
        } 
    }
    return sandwich;

}

bool Othello::EstSandwichDiagonal(Grid& grid, Position &position, char player, char enemy) const{
    int line = position.line;
    int column = position.column;

    bool sandwich = false;
    int i;
    int j;

    // EN HAUT A GAUCHE
    if(line != 0 && column != 0)
    {
        i=line-1;
        j=column-1;

        if(grid.GetCell({i,j}) == enemy)
        {
            while(i > 0 && j > 0 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i--;
                j--;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, i, j, line, column, player,1);
            }
        }
    }

    // EN HAUT A DROITE
    if(line != 0 && column != GRID_COLUMNS)
    {
        i=line-1;
        j=column+1;

        if(grid.GetCell({i,j}) == enemy)
        {
            while(i > 0 && j < GRID_COLUMNS-1 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i--;
                j++;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, i, j, line, column, player, 2);
            }
        }
    }
    // EN BAS A GAUCHE
    if(line != GRID_LINES && column != 0)
    {
        i=line+1;
        j=column-1;

        if(grid.GetCell({i,j}) == enemy)
        {
            while(i < GRID_LINES-1 && j > 0 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i++;
                j--;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, line, column, i, j, player, 2);
            }
        }
    }

    // EN BAS A DROITE
    if(line != GRID_COLUMNS && column != GRID_COLUMNS)
    {
        i=line+1;
        j=column+1;

        if(grid.GetCell({i,j}) == enemy)
        { 
            while(i < GRID_LINES-1 && j < GRID_COLUMNS-1 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i++;
                j++;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, line, column, i, j, player,1);
            }
        }
    }
    return sandwich;

}

void Othello::RetournementVertical(Grid& grid, int column, int line, int line2, char player) const{
    for(int j = line; j < line2; j++)
    {
        grid.Set({j,column}, player);        
    }

}

void Othello::RetournementHorizontal(Grid& grid, int line, int column, int column2, char player) const{ 
    for(int j = column; j < column2; j++)
    {        
        grid.Set({line, j}, player);
    }
}

void Othello::RetournementDiagonal(Grid& grid, int line, int column, int line2, int column2, char player, int diagonal) const{
    if(diagonal == 1)
    {
        for(int i = line, j = column; i < line2 && j < column2; i++, j++)
        {            
            grid.Set({i,j}, player);
        }
    }   
    else
    {
        for(int i = line, j = column; i <= line2 && j >= column2; i++, j--)
        {            
            grid.Set({i,j}, player);
        }
    }
}



