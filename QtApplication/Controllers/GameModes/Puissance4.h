#ifndef puissance4_h
#define puissance4_h

#include "../GameMode.h"
#include <memory>
#include <random>
#include <functional>

class Puissance4 : public GameMode {   
    public :
        Puissance4();                     
        inline int GetGridLineNumber() const override { return this->GRID_LINES; }
        inline int GetGridColumnNumber() const override { return this->GRID_COLUMNS; }
        inline int GetPlayerNumber() const override { return this->PLAYER_NUMBER; };
        bool IsBotEnable() const override { return true; }
        std::string GetGameName() const override { return "Puissance4"; }

        void GridInitState() const override;
        void Play() override;
        void AIPlay() const override;
        GameState GridState() const override;
        

    private :
        const int GRID_LINES = 4;
        const int GRID_COLUMNS = 7;
        const int NB_TOKEN_FOR_WIN = 4;
        const int PLAYER_NUMBER = 2;     

        Position Gravity(Position position, const Grid& grid ) const;
        bool ContainsTokenNumber(std::vector<char> gridPart, char symbol) const;
        
        bool HorizontalCheck(const Grid& grid, char symbol) const;
        bool VerticalCheck(const Grid& grid, char symbol) const;
        bool AscendingDiagonalCheck(const Grid& grid, char symbol) const;
        bool DescendingDiagonalCheck(const Grid& grid, char symbol) const;

        bool Win(const Grid& grid, char symbol) const;
        bool Equality(const Grid& grid) const;
};

#endif //puissance4_h
