#ifndef morpion_h
#define morpion_h

#include <memory>
#include <random>
#include <functional>
#include "../GameMode.h"


class Morpion : public GameMode{

    public :
        Morpion();
        inline int GetGridLineNumber() const override { return this->GRID_LINES; }
        inline int GetGridColumnNumber() const override { return this->GRID_COLUMNS; }
        inline int GetPlayerNumber() const override { return this->PLAYER_NUMBER; }
        bool IsBotEnable() const override { return true; }
        std::string GetGameName() const override { return "Morpion"; }

        void GridInitState() const override;
        void Play() override;
        void AIPlay() const override;
        GameState GridState() const override;
        

    private :
        const int GRID_LINES = 3;
        const int GRID_COLUMNS = 3;
        const int PLAYER_NUMBER = 2;

        bool Win(const Grid& grid, char symbol) const;
        bool Equality(const Grid& grid) const;
};


#endif // morpion_h
