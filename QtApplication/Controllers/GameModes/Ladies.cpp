#include "Ladies.h"
#include "../GameController.h"


//
// PUBLIC
//

Ladies::Ladies() {}

void Ladies::GridInitState() const {
    auto ctrl = GameController::GetInstance();
    Grid& grid = ctrl->GetGrid();

    // initialiser la grille de dame
    for(int i=0;i<grid.GetLineNumber();i++){
        for(int j=0;j<grid.GetColumnNumber();j++){
            // de la ligne 0 à la ligne 3 placer les pions X quand c'est pair
            // de la ligne 6 à la ligne 9 placer les pions O quand c'est pair
            if(i<4 && i%2!=0 && j%2==0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[0].GetSymbol());
            }
            else if(i<4 && i%2==0 && j%2!=0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[0].GetSymbol());
            }
            else if(i>5 && i%2!=0 && j%2==0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[1].GetSymbol());
            }
            else if(i>5 && i%2==0 && j%2!=0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[1].GetSymbol());
            }
        }
    }
}

void Ladies::Play() {
    auto ctrl = GameController::GetInstance();
    Grid& grid = ctrl->GetGrid();
    Position base_pos;

    chose_pos:
    if(same_player){
        base_pos = prev_pos;
        same_player = false;
    } else {
        base_pos = GameController::GetInstance()->RequestInputByXY();
        while(tolower(grid.GetCell(base_pos)) != ctrl->GetCurrentPlayer().GetSymbol()){
            base_pos = GameController::GetInstance()->RequestInputByXY();
        }
    }

    bool isSpecial = false;
    if(grid.isSymbolSpecial(base_pos)){
        isSpecial = true;
    }
    std::vector<std::pair<Position,Position>> neighs = std::vector<std::pair<Position,Position>>();
    bool canAttack = false;
    auto base_neigh = GetNeighs(ctrl,base_pos,isSpecial);

    printf("--\n");
    // check for adversary
    for(auto& neighbor : base_neigh){
        if(grid.GetCell(neighbor) != ctrl->GetCurrentPlayer().GetSymbol() && grid.GetCell(neighbor) != ' '){ // si adversaire
            // getting attack angle
            Position target_pos;
            auto relative = GetRelativesDirections(base_pos,neighbor);
            switch (relative) {
                case UPL:
                    target_pos = grid.GetUpLeftPosition(neighbor);
                    break;
                case UPR:
                    target_pos = grid.GetUpRightPosition(neighbor);
                    break;
                case DOWNL:
                    target_pos = grid.GetDownLeftPosition(neighbor);
                    break;
                case DOWNR:
                    target_pos = grid.GetDownRightPosition(neighbor);
                    break;
            }
            // adding to known neighbors
            if(grid.GetCell(target_pos) == ' '){
                neighs.emplace_back(target_pos, neighbor);
                canAttack = true;
                printf("Can attack\n");
            }
        }
    }
    // if no attack possible
    if(!canAttack){
        for(auto& neighbor : base_neigh){
            if(grid.GetCell(neighbor) == ' '){
                neighs.emplace_back(neighbor,Position(666,666));
            }
        }
    }

    printf("size: %zu\n",neighs.size());

    if(neighs.empty()){
        goto chose_pos;
    }

    for(auto& neigh: neighs){
        auto relative = GetRelativesDirections(base_pos,neigh.first);
        printf("{%d,%d} // %d\n",neigh.first.line,neigh.first.column,relative);
    }

    // check if the chosen pos is in the valid pos
    auto is_in_valid = [&](Position a) {
        for(auto& other : neighs){
            if( other.first.column == a.column && other.first.line == a.line ){
                return true;
            }
        }
        return false;
    };
    auto GetTargetPos = [&](Position a) {
        for(auto& other : neighs){
            if( other.first.column == a.column && other.first.line == a.line ){
                return other.second;
            }
        }
        return Position(666,666);
    };

    // request next pos
    Position next_pos = GameController::GetInstance()->RequestInputByXY();
    printf("check : %d\n",!is_in_valid(next_pos));

    printf("grid : %d\n",grid.GetCell(next_pos) != ' ');
    while(grid.GetCell(next_pos) != ' ' || !is_in_valid(next_pos)){
        printf("not valid\n");
        next_pos =GameController::GetInstance()->RequestInputByXY();
    }

    if(isSpecial){
        grid.Set(next_pos, toupper(ctrl->GetCurrentPlayer().GetSymbol()));
    }else{
        grid.Set(next_pos,ctrl->GetCurrentPlayer().GetSymbol());
    }
    grid.Set(base_pos,' ');

    auto victim = GetTargetPos(next_pos);
    if(canAttack && victim.line != 666){
        grid.Set(victim,' ');
    }

    // transform in a distinguished lady
    if(ctrl->IsFirst(ctrl->GetCurrentPlayer()) && !isSpecial){
        if(next_pos.line == ctrl->GetGrid().GetLineNumber() - 1){
            grid.ToSpecialSymbol(next_pos);
        }
    }
    else {
        if(next_pos.line == 0){
            grid.ToSpecialSymbol(next_pos);
        }
    }

    if(this->CanAttack(ctrl,next_pos,isSpecial) && canAttack) {
        printf("canAttack :%d\n",this->CanAttack(ctrl,next_pos,isSpecial));
        ctrl->SamePlayerForNextRound();
        same_player = true;
        prev_pos = next_pos;
    }
}

void Ladies::AIPlay() const {

}

GameState Ladies::GridState() const {
    auto ctrl = GameController::GetInstance();
    if(ctrl->GetGrid().GetSymbolCount(ctrl->GetCurrentPlayer().GetSymbol()) == 0) return GameState::DEFEAT;
    return GameState::NOTHING;
}


//
// Private
//



Ladies::relatives Ladies::GetRelativesDirections(Position a, Position target){
    auto relative = Position(target.line - a.line, target.column - a.column);
    //auto relative = Position(a.line - target.line, a.column - target.column);
    if(relative.line < 0 && relative.column > 0) return UPR;
    else if(relative.line < 0 && relative.column < 0) return UPL;
    else if(relative.line > 0 && relative.column > 0) return DOWNR;
    else if(relative.line > 0 && relative.column < 0) return DOWNL;
    return NONE;
}

bool Ladies::CanAttack(std::shared_ptr<GameController> ctrl,const Position& pos,bool isSpecial) {
    Grid& grid = ctrl->GetGrid();
    auto base_neigh = GetNeighs(ctrl,pos,isSpecial);
    printf("--\n");
    // check for adversary
    for(auto& neighbor : base_neigh){
        if(grid.GetCell(neighbor) != ctrl->GetCurrentPlayer().GetSymbol() && grid.GetCell(neighbor) != ' '){ // si adversaire
            // getting attack angle
            Position target_pos;
            auto relative = GetRelativesDirections(pos,neighbor);
            switch (relative) {
                case UPL:
                    target_pos = grid.GetUpLeftPosition(neighbor);
                    break;
                case UPR:
                    target_pos = grid.GetUpRightPosition(neighbor);
                    break;
                case DOWNL:
                    target_pos = grid.GetDownLeftPosition(neighbor);
                    break;
                case DOWNR:
                    target_pos = grid.GetDownRightPosition(neighbor);
                    break;
                case NONE:
                    printf("nique ta mère\n");
                    break;
            }
            // adding to known neighbors
            if(grid.GetCell(target_pos) == ' '){
                printf("Can attack\n");
                return true;
            }
        }
    }
    return false;
}
std::vector<Position> sanitize_full_diag(const Grid& grid, const std::vector<Position>& list){
    int index;
    std::vector<Position> res;
    for(int i = 0; i < list.size(); i++){
        if(grid.GetCell(list[i+1]) == '\t'){
            index = i;
            for(int i = 0 ; i <= index; i++){
                res.push_back(list[i]);
            }
            break;
        }
        else if(grid.GetCell(list[i+1]) != ' '){
            index = i;
            res.push_back(list[index-1]);
            res.push_back(list[index]);
            res.push_back(list[index+1]);
            break;
        }
    }
    return res;
}

std::vector<Position> Ladies::GetNeighs(std::shared_ptr<GameController> ctrl,Position pos, bool isSpecial) {
    Grid& grid = ctrl->GetGrid();
    std::vector<Position> base_neigh;
    if(!isSpecial){
        // getting 2 diag neighs, top or bottom accordingly
        base_neigh = (ctrl->IsFirst(ctrl->GetCurrentPlayer())) ? grid.GetDownNeighborsPosition(pos) : grid.GetUpNeighborsPosition(pos);
    }else {
        std::vector<std::pair<Position,Position>> neighs = std::vector<std::pair<Position,Position>>();
        auto ascendleft = sanitize_full_diag(grid,grid.GetNeighPosFromAscendingDiagonalLeft(pos));
        auto ascendright = sanitize_full_diag(grid,grid.GetNeighPosFromAscendingDiagonalRight(pos));
        auto descendleft = sanitize_full_diag(grid,grid.GetNeighPosFromDescendingDiagonalLeft(pos));
        auto descendright = sanitize_full_diag(grid,grid.GetNeighPosFromDescendingDiagonalRight(pos));

        base_neigh.insert(base_neigh.end(),ascendleft.begin(),ascendleft.end());
        base_neigh.insert(base_neigh.end(),ascendright.begin(),ascendright.end());
        base_neigh.insert(base_neigh.end(),descendleft.begin(),descendleft.end());
        base_neigh.insert(base_neigh.end(),descendright.begin(),descendright.end());

    }
    printf("size: %zu\n",base_neigh.size());
    for(int i = base_neigh.size() -1; i >= 0; i--){
        auto neigh = base_neigh[i];
        if(grid.GetCell(neigh) == ctrl->GetCurrentPlayer().GetSymbol()) {
            base_neigh.erase(base_neigh.begin() + i);
        }
    }
    return base_neigh;
}