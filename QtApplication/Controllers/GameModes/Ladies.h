#ifndef ladies_h
#define ladies_h
#include "../GameMode.h"

class Ladies: public GameMode {
public:
    enum relatives {
        UPL,
        UPR,
        DOWNL,
        DOWNR,
        NONE
    };
    Ladies();
    int GetGridLineNumber() const override {return this->GRID_LINES;}
    int GetGridColumnNumber() const override {return this->GRID_COLUMNS;}
    int GetPlayerNumber() const override {return this->PLAYER_NUMBER;}
    bool IsBotEnable() const override { return false; }
    std::string GetGameName() const override { return "Ladies"; }

    void GridInitState() const override;
    void Play() override;
    void AIPlay() const override;
    GameState GridState() const override;

private:
    const int GRID_LINES = 10;
    const int GRID_COLUMNS = 10;
    const int PLAYER_NUMBER = 2;

    bool same_player = false;
    Position prev_pos;

    bool CanAttack(std::shared_ptr<GameController> ctrl,const Position&,bool isSpecial);

    relatives GetRelativesDirections(Position a, Position target);
    std::vector<Position> GetNeighs(std::shared_ptr<GameController> ctrl,Position pos, bool isSpecial);
};
#endif // ladies_h
