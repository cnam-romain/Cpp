#ifndef CLI_h
#define CLI_h

#include "../UserInterface.h"

class CLI : public UserInterface {

    public :
        
        void DisplayStartMenu() const override;
        void DisplayGameMenu() const override;
        void DisplayPlayerMenu(bool isBotEnable) const override;
        void DisplayGridView(int gridLines, int gridColumns) const override;
        void DisplayFinalState(std::string label) const override;
        void DisplayEndMenu() const override;

        void DisplayRequestInputByXY(int width, int height) const override;
        void DisplayRequestInputByCols(int width) const override;
        void DisplayRequestInputByLine(int height) const override;

        void DisplayMainMessage(std::string message) const override;
        void DisplayMessage(std::string message) const override;
        void DisplayWarningMessage(std::string message) const override;
        void DisplayGrid(const Grid& grid) override;

    private :
        int ReadInt() const;
        char ReadChar() const;        
};


#endif //CLI_h
