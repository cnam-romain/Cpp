
#include <iostream>
#include "CLI.h"
#include "../../Controllers/GameController.h"

//
// Public
//

void CLI::DisplayStartMenu() const {
    std::cout << "(0) Nouvelle partie\n(1) Charger une sauvegarde\n > ";

    int choice;
    bool isCorrectInput = false;
    while(!isCorrectInput){
        choice = ReadInt();
        if(choice >= 0 && choice < 2){
            isCorrectInput  = true;
        }
        else{
            std::cout << "Entrer un nombre entre 0 et 1\n > "; 
        }
    }

    if(choice == 0){       
        emit GameController::GetInstance()->CreateNewGame_Signal();
    }
    else{
        std::cout << "path > "; 
        std::string str;
        std::cin >> str;
        emit GameController::GetInstance()->LoadGame_Signal(str);
    }
}

void CLI::DisplayGameMenu() const {    
    std::cout << "(0) Morpion\n(1) Puissance4\n(2) Othello\n(3) Ladies\n > ";
    
    int choice;
    bool isCorrectInput = false;
    while(!isCorrectInput){
        choice = ReadInt();
        if(choice >= 0 && choice < 4){
            isCorrectInput  = true;
        }
        else{
            std::cout << "Entrer un nombre entre 0 et 3\n > ";
        }
    }

    emit GameController::GetInstance()->GameChosed_Signal(choice);
}
void CLI::DisplayPlayerMenu(bool isBotEnable) const {
    std::string nameP1;
    std::string nameP2;
    bool isBotP1;
    bool isBotP2;

    std::cout << "Joueur 1\n > ";
    std::cin >> nameP1;
    if(isBotEnable){
        std::cout << "Bot ? (0:non | 1:oui)\n > ";
        isBotP1 = ReadInt()==1;
    }
    std::cout << "Joueur 2\n > ";
    std::cin >> nameP2;
    if(isBotEnable){
        std::cout << "Bot ? (0:non | 1:oui)\n > ";
        isBotP2 = ReadInt()==1;
    }

    emit GameController::GetInstance()->CreatePlayer_Signal(nameP1, isBotP1, nameP2, isBotP2);

}
void CLI::DisplayGridView(int gridLines, int gridColumns ) const {

}
void CLI::DisplayFinalState(std::string label) const {
    DisplayMainMessage(label);
    emit GameController::GetInstance()->GoToEndPage_Signal();
}
void CLI::DisplayEndMenu() const {
    std::cout << "(0) Rejouer à la même partie\n(1) Rejouer avec d'autres joueurs\n(2) Jouer à un autre jeu\n(3) Quitter\n > ";

    int choice;
    bool isCorrectInput = false;
    while(!isCorrectInput){
        choice = ReadInt();
        if(choice >= 0 && choice < 4){
            isCorrectInput  = true;
        }
        else{
            std::cout << "Entrer un nombre entre 0 et 3\n > ";
        }
    }

    switch (choice) {
        case 0:
            emit GameController::GetInstance()->Replay_Signal();
            break;
        case 1:
            emit GameController::GetInstance()->ReplayWithNewPlayers_Signal();
            break;
        case 2:
            emit GameController::GetInstance()->CreateNewGame_Signal();
            break;
        default:
            emit GameController::GetInstance()->Exit_Signal();
            break;
    }
}


void CLI::DisplayRequestInputByXY(int width, int height) const {
    int line, column;
    do{
        std::cout << "Ligne [ 1 - "<< height << " ] " << std::endl << " > ";
        line = ReadInt();
    }while (line < 1 || line > height);    
    
    do{
        std::cout << "Colonne [ 1 - "<< width << " ] " << std::endl << " > ";
        column = ReadInt();
    }while(column < 1 || column > width);

    emit GameController::GetInstance()->PlayerInput_Signal(Position(line-1, column-1));
}

void CLI::DisplayRequestInputByCols(int width) const {
    int column;
    do{
        std::cout << "Colonne [ 1 - "<< width << " ] "<< std::endl << " > ";
        column = ReadInt();
    }while( column < 1 || column > width);

    emit GameController::GetInstance()->PlayerInput_Signal(Position(0, column-1));   ;
}

void CLI::DisplayRequestInputByLine(int height) const {
    int line;
    do{
        std::cout << "Ligne [ 1 - "<< height << " ] "<< std::endl << " > ";
        line = ReadInt();
    }while(line < 1 || line > height);
        
    emit GameController::GetInstance()->PlayerInput_Signal(Position(line-1, 0));
}

void CLI::DisplayMainMessage(std::string message) const {
    std::cout << message << std::endl;
}

void CLI::DisplayMessage(std::string message) const {
    std::cout << message << std::endl;
}

void CLI::DisplayWarningMessage(std::string message) const {
    std::cout << message << std::endl;
}

void CLI::DisplayGrid(const Grid& grid) {
    std::cout << std::endl;
    // Column header
    std::cout<< "  ";
    for(int i=1; i<=grid.GetColumnNumber(); i++){
        std::cout << " " << i;
    }
    std::cout << std::endl;

    for(int line=0; line<grid.GetLineNumber(); line++){
        std::cout << line+1 << " |";
        for(char cell : grid.GetCellContentFromLine(line)){
            std::cout << cell << "|";
        }
        std::cout << std::endl;
    }
}


//
// Private
//

int CLI::ReadInt() const {
    int value;
    do{
        std::cin >> value;
        if(value == 100){
            // ctrl->RestartGame();
        }
        if(std::cin.fail()){
            std::cout << "Entrer un nombre " << std::endl << " > ";
            std::cin.clear();
            std::cin.ignore( 10000, '\n' );        
        }
        else
            break;
            

    }while(true);    
    return value;
}

char CLI::ReadChar() const {
    char value;
    std::cin.clear();
    do{
        std::cin.clear();
        std::cin >> value;
        if(std::cin.fail()){
            std::cout << "Entrer un caractère" << std::endl << " > ";
        }
        else break;
    }while(true);   
    return value;
}

