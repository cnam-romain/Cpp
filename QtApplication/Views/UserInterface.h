#ifndef UserInterface_h
#define UserInterface_h

#include "../Models/Structs.h"
#include "../Models/Grid.h"
#include "../Models/Player.h"

class GameController;

class UserInterface
{
    public :

        virtual void DisplayStartMenu() const = 0;
        virtual void DisplayGameMenu() const = 0;
        virtual void DisplayPlayerMenu(bool isBotEnable) const = 0;
        virtual void DisplayGridView(int gridLines, int gridColumns) const = 0;        
        virtual void DisplayFinalState(std::string label) const = 0;
        virtual void DisplayEndMenu() const = 0;

        virtual void DisplayRequestInputByXY(int width, int heigth) const = 0;
        virtual void DisplayRequestInputByCols(int width) const = 0;
        virtual void DisplayRequestInputByLine(int heigth) const = 0;
    
        virtual void DisplayMainMessage(std::string message) const = 0;
        virtual void DisplayMessage(std::string message) const = 0;
        virtual void DisplayWarningMessage(std::string message) const = 0;

        virtual void DisplayGrid(const Grid& grid) = 0;        
                        
};


#endif // UserInterface_h

