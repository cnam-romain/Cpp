#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../UserInterface.h"
#include "Controllers/GameController.h"
#include <QMainWindow>
#include "qgridlayout.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow , public UserInterface
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void DisplayStartMenu() const override;
    void DisplayGameMenu() const override;
    void DisplayPlayerMenu(bool isBotEnable) const override;
    void DisplayGridView(int gridLines, int gridColumns) const override;
    void DisplayFinalState(std::string label) const override;
    void DisplayEndMenu() const override;

    void DisplayRequestInputByXY(int width, int height) const override;
    void DisplayRequestInputByCols(int width) const override;
    void DisplayRequestInputByLine(int height) const override;

    void DisplayMainMessage(std::string message) const override;
    void DisplayMessage(std::string message) const override;
    void DisplayWarningMessage(std::string message) const override;
    void DisplayGrid(const Grid& grid) override;

    //void QDosDane(bool& bill) const;

private slots:

    void on_quitbtn_clicked();


    void on_nouvellepartiebtn_clicked();

    void on_morpionbtn_clicked();

    void on_puissance4btn_clicked();

    void on_othellobtn_clicked();

    void on_ladiesbtn_clicked();

    void on_pushButton_2_clicked();

    void on_chargerpartiebtn_clicked();

    void on_nextbtn_clicked();

    void on_rjouerbtn_clicked();

    void on_rejouer2btn_clicked();

    void on_autrejeubtn_clicked();

    void on_quitterbtn_clicked();

    void on_savebtn_clicked();

private:
    const int START_MENU_PAGE_INDEX = 1;
    const int GAME_MENU_PAGE_INDEX = 0;
    const int PLAYER_MENU_PAGE_INDEX = 2;
    const int GRID_PAGE_INDEX = 3;
    const int END_MENU_PAGE_INDEX = 4;

    Ui::MainWindow *ui;           
    void ClearGridLayout() const;
    void OpenFileForLoad();
    void Quit();
};
#endif // MAINWINDOW_H
