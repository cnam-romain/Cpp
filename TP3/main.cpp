
#include <stdlib.h>
#include "include/Controllers/GameController.h"
#include "include/Controllers/CLI.h"

int main(){
    
    GameController gameCtrl;
    CLI userInterface;

    gameCtrl.SetUserInterface( std::make_shared<CLI>(userInterface) );
    gameCtrl.Play();
      
    return EXIT_SUCCESS;

}