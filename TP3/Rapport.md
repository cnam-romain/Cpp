# Rapport

Les 2 projets étaient assez similaires sur leur fonctionnement mais on a choisi celui qui respectait le plus les principes de la POO (La POO c'est nul de toute façon (C'est Paul qui le dit)).

<br>

## Modifications

On a repensé le fonctionnement de notre design pattern Strategy afin d'avoir une classe que l'on peut considérer plus comme une classe de "configuration" qui représente le fonctionnement, les règles du jeu.

On a identifié 5 composantes qui définissent un jeu: 
+ Le nombre et la composition des joueurs qui y jouent
+ La condition de victoire 
+ La condition d'égalité
+ Les dimensions de la grille
+ La manière dont on place des pièces sur la grille

Cette classe contient toutes les données nécessaires pour définir le jeu.
On y retrouve alors principalement des getters, pour décrire quelles données doivent être mises à disposition de la bibliothèque, l'utilisateur est libre de l'implémentation.

<br>

## Responsabilités

Ce refactoring nous permet de réduire les responsabilités de la classe Strategy précédente (GameStrategy) et de pouvoir profiter d'un code plus modulable.

<br>

## Modularité
Le principe de cette modularité est de pouvoir réutiliser des briques logiques sur de nouveaux jeux à implémenter. Si un nouveau jeu utilise le même système d'input que le Morpion, il ne sera pas nécessaire de le recoder et donc on limite aussi les doublons de code.

<br>

## Abstractions
Le code précédent ne bénéficiait pas d'une abstraction du joueur, ne permettant pas d'ajouter plusieurs type de joueur (IA, joueur avec des aides au placement, ...) 
(non respect du O de SOLID). La solution, une interface Player, qui implémentent HumanPlayer et AIPlayer.
De la même manière le mode de placement était hard codé, ne permettant pas d'ajouter d'autres modes de placement (par lignes, colonnes, ...), la solution choisie ici aussi, est l'ajout d'une interface.
Idem pour le comportement de la pièce lors du placement.