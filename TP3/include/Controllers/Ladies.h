#ifndef ladies_h
#define ladies_h
#include "Interfaces/GameMode.h"

class Ladies: public GameMode {
public:
    Ladies();
    int GetGridLineNumber() const override {return this->GRID_LINES;}
    int GetGridColumnNumber() const override {return this->GRID_COLUMNS;}
    int GetPlayerNumber() const override {return this->PLAYER_NUMBER;}

    void GridInitState(GameController* ctrl) const override;
    void Play( GameController* ctrl) const override;
    void AIPlay( GameController* ctrl) const override;
    GameState GridState( GameController* ctrl) const override;

private:
    const int GRID_LINES = 10;
    const int GRID_COLUMNS = 10;
    const int PLAYER_NUMBER = 2;

    bool Win(const Grid& grid, char symbol) const;
    bool Equality(const Grid& grid, char symbol) const;
    bool CheckIsValidPosition(const Position &pos) const;
    bool CanAttack(GameController *ctrl) const ;
    void DelDouble(std::vector<Position>&) const;
    void NormalPlay(GameController* ctrl, Position base_pos) const;
    void SpecialPlay(GameController* ctrl, Position base_pos) const;
};
#endif // ladies_h