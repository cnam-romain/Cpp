#ifndef UserInterface_h
#define UserInterface_h

#include "../../Models/Structs.h"
#include "../../Models/Grid.h"
#include "../../Models/Player.h"

class GameController;

class UserInterface
{
    public :

        virtual void RegisterController(GameController* ctrl) = 0;

        virtual Position GetInputByXY(int width, int heigth) const = 0;
        virtual Position GetInputByCols(int width) const = 0;
        virtual Position GetInputByLine(int heigth) const = 0;

        virtual std::string GetUserStringInput(std::string label) const = 0;
        virtual std::string GetUserFile(std::string label) const = 0;                

        virtual int DisplayChoiceList(std::vector<std::string> choiceList) = 0;

        virtual void DisplayMessage(std::string message) const = 0;

        virtual void DisplayGrid(const Grid& grid) = 0;
        virtual void DisplayCurrentPlayer(const Player& player) const = 0;
        virtual void DisplayWinnerMessage(const Player& player) const = 0;
        virtual void DisplayEqualityMessage() const = 0;
        virtual void ClearInterface() = 0;
};


#endif // UserInterface_h

