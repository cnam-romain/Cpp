#ifndef GAME_OTHELLO_H
#define GAME_OTHELLO_H

#include "Interfaces/GameMode.h"

class Othello: public GameMode {
    public:
        Othello();
        int GetGridLineNumber() const override;
        int GetGridColumnNumber() const override;
        int GetPlayerNumber() const override;

        void GridInitState(GameController* ctrl) const override;
        void Play(GameController* ctrl) const override;
        void AIPlay(GameController* ctrl) const override;
        GameState GridState(GameController* ctrl) const override;        


    private:
        const int GRID_LINES = 8;
        const int GRID_COLUMNS = 8;
        const int PLAYER_NUMBER = 2;


        bool Win(const Grid& grid, char symbol, char enemy) const;
        bool Equality(const Grid& grid, char symbol) const;  

        bool EstSandwichVertical(Grid& grid, Position &position, char player, char enemy) const;
        bool EstSandwichHorizontal(Grid& grid, Position &posGetOtherPlayerition, char player, char enemy) const;
        bool EstSandwichDiagonal(Grid& grid, Position &position, char player, char enemy) const;

        void RetournementVertical(Grid& grid, int column, int line, int line2, char player) const;
        void RetournementHorizontal(Grid& grid, int line, int column, int column2, char player) const;
        void RetournementDiagonal(Grid& grid, int line, int column, int line2, int column2, char player, int diagonal) const;


};

#endif //GAME_OTHELLO_H
