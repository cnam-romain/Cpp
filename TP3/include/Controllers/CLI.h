#ifndef CLI_h
#define CLI_h

#include "Interfaces/UserInterface.h"
#include "GameController.h"

class CLI : public UserInterface {

    public :
        
        void RegisterController(GameController* ctrl) override;
        
        Position GetInputByXY(int width, int height) const override;
        Position GetInputByCols(int width) const override;
        Position GetInputByLine(int height) const override;

        std::string GetUserStringInput(std::string label) const override;
        std::string GetUserFile(std::string label) const override;

        int DisplayChoiceList(std::vector<std::string> choiceList) override;

        void DisplayMessage(std::string message) const override;
        
        void DisplayGrid(const Grid& grid) override;
        void DisplayCurrentPlayer(const Player& player) const override;
        void DisplayWinnerMessage(const Player& player) const override;
        void DisplayEqualityMessage() const override;
        void ClearInterface() override;

    private :
        int ReadInt() const;
        char ReadChar() const;
        GameController* ctrl;
};


#endif //CLI_h