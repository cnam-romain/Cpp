#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Controllers/Interfaces/UserInterface.h"
#include "qgridlayout.h"
#include "Controllers/GameController.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow , public UserInterface
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void RegisterController(GameController* ctrl) override;
    Position GetInputByXY(int width, int height) const override;
    Position GetInputByCols(int width) const override;
    Position GetInputByLine(int height) const override;

    std::string GetUserStringInput(std::string label) override;
    std::string GetUserFile(std::string label) override;


    int DisplayChoiceList(std::vector<std::string> choiceList) override;

    void DisplayMessage(std::string message) const override;

    void DisplayGrid(const Grid& grid) override;
    void DisplayCurrentPlayer(const Player& player) const override;
    void DisplayWinnerMessage(const Player& player)const  override;
    void DisplayEqualityMessage()const override;
    void ClearInterface() override;
    void OpenFile();

    //void QDosDane(bool& bill) const;

private slots:
    void on_yoDeryabtn_clicked();

    void on_yoRomainbtn_clicked();

private:
    Ui::MainWindow *ui;
    std::shared_ptr<QGridLayout> gridLayout;
    GameController *gamectrl;
    void UpdateWindow();
    void Quit();
};
#endif // MAINWINDOW_H
