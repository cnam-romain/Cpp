#ifndef SAUVEGARDE_H
#define SAUVEGARDE_H

#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QFile>
#include <QtCore/QJsonArray>


class Sauvegarde
{
    public:
        Sauvegarde();

        QJsonObject Charger(const QString& fichier);
        void Sauvegarder( QJsonObject obj, const QString& fichier);
};

#endif // SAUVEGARDE_H
