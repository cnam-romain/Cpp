#ifndef player_h
#define player_h

#include <iostream>
#include <cstring>

class Player {
    public:
        Player(){}   
        Player(std::string name, char symbol, bool isBot){
            this->name = name;
            this->symbol = symbol;
            this->isBot = isBot;
        } 

        char inline GetSymbol() const { return this->symbol; }
        std::string inline GetName() const { return this->name; }
        bool inline IsBot() const { return this->isBot; }

    private:
        std::string name;
        char symbol;
        bool isBot;

};


#endif //player_h
