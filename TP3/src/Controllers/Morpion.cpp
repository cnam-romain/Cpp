#include "../../include/Controllers/Morpion.h"
#include "../../include/Controllers/GameController.h"

Morpion::Morpion(){
    std::cout << "Game mode = Morpion" << std::endl << std::endl;
}

void Morpion::Play( GameController* ctrl) const{    
    std::shared_ptr<UserInterface> userInterface = ctrl->GetUserInterface();
    Grid& grid = ctrl->GetGrid();

    Position pos;
    bool isCorrectInput = false;
    while(!isCorrectInput){
        pos = userInterface->GetInputByXY(GRID_LINES, GRID_COLUMNS);
        if(grid.IsEmpty(pos)){
            isCorrectInput = true;
        }
        else{
            userInterface->DisplayMessage("Position incorrect\n");
        }
    }
    
    grid.Place(pos, ctrl->GetCurrentPlayer().GetSymbol());
}

void Morpion::AIPlay( GameController* ctrl) const {
    Grid& grid = ctrl->GetGrid();

    std::random_device r;
    std::vector<Position> emptyCells =grid.GetEmptyCells();
    auto gen = std::bind(std::uniform_int_distribution<>(0,emptyCells.size()-1),std::default_random_engine(r()));    
        
    Position pos;    
    do{
        pos = emptyCells[gen()];
    }while(!grid.IsEmpty(pos));
    
    grid.Place(pos, ctrl->GetCurrentPlayer().GetSymbol());    
}

GameState Morpion::GridState(GameController* ctrl) const {
    if(Win(ctrl->GetGrid(), ctrl->GetCurrentPlayer().GetSymbol())){
        return GameState::WIN;
    }
    else if(Equality(ctrl->GetGrid())){
        return GameState::EQUALITY;
    }
    return GameState::NOTHING;    
}

void Morpion::GridInitState(GameController* ctrl) const {
    
}


//
// PRIVATE
//

bool Morpion::Win(const Grid& grid, char symbol) const {
    int win[8][3] = {{0, 1, 2}, // Check first row.
                     {3, 4, 5}, // Check second Row
                     {6, 7, 8}, // Check third Row
                     {0, 3, 6}, // Check first column
                     {1, 4, 7}, // Check second Column
                     {2, 5, 8}, // Check third Column
                     {0, 4, 8}, // Check first Diagonal
                     {2, 4, 6}}; // Check second Diagonal
    // Check all possible winning combinations
    for (auto & i : win){
        auto a = grid.GetCell(i[0]);
        auto b = grid.GetCell(i[1]);
        auto c = grid.GetCell(i[2]);
        if (a == b && b == c && a == symbol){
            return true;
        }
    }
    return false;
}

bool Morpion::Equality(const Grid& grid) const {
    return grid.GetEmptyCells().empty();
}
