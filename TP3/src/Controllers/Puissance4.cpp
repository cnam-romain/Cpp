#include "../../include/Controllers/Puissance4.h"
#include "../../include/Controllers/GameController.h"

//
// Public
//

Puissance4::Puissance4(){
    std::cout << "Game mode = Puissance4" << std::endl << std::endl;
}

void Puissance4::Play( GameController* ctrl) const {
    std::shared_ptr<UserInterface> userInterface = ctrl->GetUserInterface();
    Grid& grid = ctrl->GetGrid();

    Position pos;
    bool isCorrectInput = false;
    while (!isCorrectInput)
    {
        pos = userInterface->GetInputByCols(GRID_COLUMNS);
        if(grid.IsEmpty(pos)){
            isCorrectInput = true;
        }
        else{
            userInterface->DisplayMessage("Position incorrect\n");
        }
    }
    pos = Gravity(pos, grid);

    grid.Place(pos, ctrl->GetCurrentPlayer().GetSymbol());    
}

void Puissance4::AIPlay( GameController* ctrl) const {
    Grid& grid = ctrl->GetGrid();

    std::random_device r;
    std::vector<Position> emptyCells = grid.GetEmptyCells();
    std::vector<Position> emptyColumns(emptyCells.size());
    auto it = std::copy_if(
            emptyCells.begin(),
            emptyCells.end(),
            emptyColumns.begin(),
            [](Position pos)->bool { return pos.line == 0; }
    );
    emptyColumns.resize(std::distance(emptyColumns.begin(), it));

    auto gen = std::bind(std::uniform_int_distribution<>(0,emptyColumns.size()-1),std::default_random_engine(r()));       
    Position pos;
    do{
        pos = emptyColumns[gen()];
    }while (!grid.IsEmpty(pos));
    pos = Gravity(pos,grid);

    grid.Place(pos, ctrl->GetCurrentPlayer().GetSymbol());
}

GameState Puissance4::GridState( GameController* ctrl) const {    
    if(Win(ctrl->GetGrid(), ctrl->GetCurrentPlayer().GetSymbol())){
        return GameState::WIN;
    }
    else if( Equality(ctrl->GetGrid())){
        return GameState::EQUALITY;
    }
    return GameState::NOTHING;
}


void Puissance4::GridInitState(GameController* ctrl) const {
    
}


//
// Private
//

bool Puissance4::Win(const Grid& grid, char symbol) const {
    return 
        HorizontalCheck(grid, symbol) ||
        VerticalCheck(grid, symbol) ||
        AscendingDiagonalCheck(grid, symbol) ||
        DescendingDiagonalCheck(grid, symbol);
}

bool Puissance4::Equality(const Grid& grid) const {
    return grid.GetEmptyCells().size() <= 0;
}

Position Puissance4::Gravity(Position position, const Grid& grid ) const{
    while (position.line<grid.GetLineNumber()-1)
    {
        position.line++;
        if( !grid.IsEmpty( position ) ){
            position.line--;
            break;
        }    
    }
    return position; 
}

bool Puissance4::ContainsTokenNumber(std::vector<char> gridPart, char symbol) const{
    int count = 0;
    for(char element : gridPart){
        if(element == symbol){ count++;}
        else{count = 0;}

        if(count >= NB_TOKEN_FOR_WIN){ return true; }
    }
    return false;
}

bool Puissance4::HorizontalCheck(const Grid& grid, char symbol) const {
    for(int line=0; line<GRID_LINES; line++){
        if( ContainsTokenNumber( grid.GetCellContentFromLine(line), symbol ) ){
            return true;
        }        
    }
    return false;
}

bool Puissance4::VerticalCheck(const Grid& grid, char symbol) const {
    for(int column=0; column<GRID_COLUMNS; column++){
        if( ContainsTokenNumber( grid.GetCellContentFromColumn(column), symbol ) ){
            return true;
        }        
    }
    return false;
}

bool Puissance4::AscendingDiagonalCheck(const Grid& grid, char symbol) const{
    for(int column=0; column <= GRID_COLUMNS-NB_TOKEN_FOR_WIN; column++){
        if( ContainsTokenNumber(
            grid.GetCellContentFromAscendingDiagonal(Position(GRID_LINES-1, column)),
            symbol)
        ){
            return true;
        }
    }
    return false;
}

bool Puissance4::DescendingDiagonalCheck(const Grid& grid, char symbol) const{
    for(int column=0; column <= GRID_COLUMNS-NB_TOKEN_FOR_WIN; column++){
        if( ContainsTokenNumber(
            grid.GetCellContentFromDescendingDiagonal(Position(0, column)),
            symbol)
        ){
            return true;
        }
    }
    return false;
}

