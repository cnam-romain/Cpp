
#include <iostream>
#include "../../include/Controllers/CLI.h"

//
// Public
//
Position CLI::GetInputByXY(int width, int height) const {
    int line, column;
    do{
        std::cout << "Ligne [ 1 - "<< height << " ] " << std::endl << " > ";
        line = ReadInt();
    }while (line < 1 || line > height);    
    
    do{
        std::cout << "Colonne [ 1 - "<< width << " ] " << std::endl << " > ";
        column = ReadInt();
    }while(column < 1 || column > width);

    return Position(line-1, column-1);    
}

Position CLI::GetInputByCols(int width) const {
    int column;
    do{
        std::cout << "Colonne [ 1 - "<< width << " ] "<< std::endl << " > ";
        column = ReadInt();
    }while( column < 1 || column > width);

    return Position(0, column-1);
}

Position CLI::GetInputByLine(int height) const {
    int line;
    do{
        std::cout << "Ligne [ 1 - "<< height << " ] "<< std::endl << " > ";
        line = ReadInt();
    }while(line < 1 || line > height);
    
    return Position(line-1, 0);
}

std::string CLI::GetUserStringInput(std::string label) const {
    std::cout << label << " > "; 

    std::string str;
    std::cin >> str;
    return str;
}

std::string CLI::GetUserFile(std::string label) const {
    std::cout << label << " > ";

    std::string str;
    std::cin >> str;
    return str;
}

int CLI::DisplayChoiceList(std::vector<std::string> choiceList) {
    
    int index=0;
    for(std::string choice : choiceList){
        std::cout << "(" << index << ") : " << choice << std::endl;
        index++;
    }

    std::cout << " > ";

    int choice;
    bool isCorrectInput = false;
    while(!isCorrectInput){
        choice = ReadInt();
        if(choice >= 0 && choice < choiceList.size()){
            isCorrectInput  = true;
        }
        else{
            std::cout << "Entrer un nombre entre 0 et " << choiceList.size()-1 << std::endl << " > "; 
        }
    }

    return choice;    
}

void CLI::DisplayMessage(std::string message) const {
    std::cout << message;
}

void CLI::DisplayGrid(const Grid& grid) {
    std::cout << std::endl;
    // Column header
    std::cout<< "  ";
    for(int i=1; i<=grid.GetColumnNumber(); i++){
        std::cout << " " << i;
    }
    std::cout << std::endl;

    for(int line=0; line<grid.GetLineNumber(); line++){
        std::cout << line+1 << " |";
        for(char cell : grid.GetCellContentFromLine(line)){
            std::cout << cell << "|";
        }
        std::cout << std::endl;
    }
}

void CLI::DisplayCurrentPlayer(const Player& player) const {
    std::cout << " --> " << player.GetName() << "("<<player.GetSymbol() << ")" << std::endl;
}

void CLI::DisplayWinnerMessage(const Player& player) const {
    std::cout <<  " --> " << player.GetName() << " à gagné " << std::endl;
}

void CLI::DisplayEqualityMessage() const  {
    std::cout << "Egalité"<< std::endl;
}

void CLI::ClearInterface() {
    system("clear");
}

void CLI::RegisterController(GameController* ctrl){
    this->ctrl = ctrl;
}

//
// Private
//

int CLI::ReadInt() const {
    int value;
    do{
        std::cin >> value;
        if(value == 100){
            ctrl->RestartGame();
        }
        if(std::cin.fail()){
            std::cout << "Entrer un nombre " << std::endl << " > ";
            std::cin.clear();
            std::cin.ignore( 10000, '\n' );        
        }
        else
            break;
            

    }while(true);    
    return value;
}

char CLI::ReadChar() const {
    char value;
    std::cin.clear();
    do{
        std::cin.clear();
        std::cin >> value;
        if(std::cin.fail()){
            std::cout << "Entrer un caractère" << std::endl << " > ";
        }
        else break;
    }while(true);   
    return value;
}

